---
title: "My Meditations proof copy arrived"
date: "2021-06-10"
---

I've been been working on bringing the 1902 George Chrystal translation of the Meditations back into print. A few years ago when I was first interested in reading the Meditations the two translations available on Project Gutenberg were the 1902 George Chrystal and the 1634 Meric Casaubon. Casaubon's translation was the first ever English translation of the work and I find it is quite hard to read so I ended up reading the George Chrystal. This book was my first introduction to stoic philosophy.

These days the translations by Gregory Hays and George Long are by far the most popular and Chrystal's has become largely unknown. I had a hard time even finding scans of an original to use as a source. To the best of my knowledge the proof copy I have here for my new edition is the first copy of Chrystal's translation to be printed in 119 years.

![Front cover](meditations_r1_cover.jpg)

## Production quality
I've been using Lulu for printing. Other people have reported terrible experiences with them but so far I've had minimal trouble. The production quality is quite good.

![Inside pages](meditations_r1_inside.jpg)

The pages are printed on 60gsm white paper which is typical for reasonable quality paperbacks. The paper feels alright and the printing is very sharp and clear. It looks exactly the same on paper as it does on the printing document. The cover has a satin finish and is sturdy enough. The text printing on the cover isn't as sharp as on the pages and there is a slight blur around the edges of letters if you look close enough. This might just be an artifact from Lulu's cover generator which I used because I didn't want to set to with cover designs at the time. The binding is standard paperback glue binding and is quite well done. All said, it's a fairly high quality paperback and for a print cost of £4.72 it exceeds expectations.

## Design
I had this book printed in 7"x10" but now it's in front of me I'm sure it's too big. It's not that much smaller than an A4 sheet. The page formatting works out quite well in 7"x10" but the book feels too big. Before I make these available to other people I want to go down to 6"x9" and adjust the printing documents slightly to make it work well in that size. I also need to create a proper cover document. The one in the photo is just a placeholder and hopefully if I provide my own and don't use the generator the blur on the cover text will go away.

The latex `book` class has served me well in designing this. The inner and outer margins are just right at their default widths and page numbers and chapter titles are exactly where they should be. It's possible that I'll have to make the inner margins slightly wider for the 6"x9" document as more pages will mean more space lost in the binding. We'll see how that goes at the time.

The only complaint I have about the production of the book is that there is a barcode printed on the back of the last page. On the PDF proof they gave me this was not shown and I deliberately didn't add any barcodes or numbers to the back cover. I suspect this is something they automatically add if you don't have one anywhere because the manufacturing process needs it.