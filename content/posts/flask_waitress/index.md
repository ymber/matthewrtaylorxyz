---
title: "Deploying a Flask app with Waitress behind an Nginx reverse proxy"
date: "2022-04-07"
lastmod: "2022-04-07"
---

Of all the uWSGI servers I often see uwsgi recommended because it has significantly better performance than Gunicorn or Waitress and its adaptability is excellent. The often glossed over part is that configuring the thing is horrific and it's really flaky in Python environments it doesn't like. I've taken a liking to Waitress for deploying Python web apps as it's simple to work with and reliably behaves how you expect it to.

## Waitress setup
Waitress is a pure Python uWSGI server. You can use PasteDeploy with it for more complex configuration but for a basic setup there's no need. Because it's pure python you can just install it in your venv and you don't have to deal with system packages.

You'll find uwsgi flaking out if you try to use it with anything but venv but Waitress plays nice with any of the virtual environment tools I've tried it with. I'll use Pipenv here and I'll be running my knockoff pastebin straight out the git repo for the real ghetto server experience. In a production deployment you should have your WSGI application installed as a Python package.

```
git clone https://gitlab.com/ymber/paste.git
cd paste && pipenv sync
```

Take your WSGI app and `pipenv shell` into the venv. My pastebin doesn't strictly depend on Waitress so I don't keep it in the dependencies in the `Pipfile` so it will need installing here.

```
pipenv install waitress
```

Waitress has a CLI wrapper around the `waitress.serve` function that you can use to serve the application without keeping Waitress specific code in VC. A lot of other guides on deploying WSGI apps with Waitress will tell you to call `waitress.serve` from your backend code but that's no good if you want to keep it server neutral for deployment. The proper way to do it is by passing arguments to that wrapper or using PasteDeploy.

```
waitress-serve --host=localhost --port=8922 --url-scheme=https --call "paste:create_app" &
```

If you run that you'll see in the output that the application is being served over HTTP even though the command specifies `--url-scheme=https`. Waitress doesn't have any native support for HTTPS. This option exists so that when you're using it with a reverse proxy it knows that incoming requests will be HTTPS and that will be handled by the proxying server but the proper URL scheme needs to be maintained. If incoming requests might be HTTP or HTTPS there's a more complicated setup you can do with checking request headers but on this server I have redirects configured so all incoming connections will be HTTPS.

Unlike other uWSGI servers Waitress doesn't require a callable WSGI object to be passed directly to it so you can give it an app factory like I'm doing here by passing `--call`. I have an app factory function `create_app` and it knows how to get a callable WSGI object from that. There's no need for a `wsgi.py` file to instantiate your app like is conventional for deploying with uwsgi. The documentation explains [waitress-serve usage](https://docs.pylonsproject.org/projects/waitress/en/stable/runner.html) quite clearly.

You'd probably want to write a systemd service to automatically run your uWSGI server or anything else that's better than just forking that command to the background but for the demonstration case that will do. If you run that and try to connect to 127.0.0.1:8922 you should have a working deployment of your WSGI app.

## Nginx configuration
Waitress will serve the application fine on its own but for things like working HTTPS you'll need a reverse proxy set up. I run nginx on this server and it's easy to configure a virtual server proxy.

```
server {
    listen 80;
    listen [::]:80;
    
    server_name paste.matthewrtaylor.xyz www.paste.matthewrtaylor.xyz;
    
    location / {
        proxy_pass http://localhost:8922;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header Host $host;
    }
}
```

I set that config in `/etc/nginx/sites-available` and symlinked it to `/etc/nginx/sites-enabled`. Notice it's listening on port 80 for HTTP requests. Certbot will fix that later.

The part of that config particular to serving as a reverse proxy to a uWSGI server is in the `location` block. Any requests that go to this virtual server will be redirected to `http://localhost:8922`, which we configured earlier as the URL for Waitress to serve the WSGI application on. The two `proxy_set_header` lines are in place there because nginx automatically alters some headers of incoming requests before sending them on when it's acting as a proxy. Those lines will give them their original values. The nginx [documentation](https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/) explains what goes on there.

With that set up run `certbot --nginx` to have it set up certificates for the domain and adjust the nginx config to serve over HTTPS. Remember to reoad your nginx configs with `systemctl reload nginx` too.

## Proxying to a socket
It's possible to set up Waitress to serve the application on a Unix socket. It should also be possible to get nginx to work as a proxy for that socket but I haven't been able to make that work yet. The Flask documentation shows something similar in how it has the reverse proxy set up for a uwsgi deployment but when I tried to emulate how that worked I got 502s back.