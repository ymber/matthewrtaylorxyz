---
title: "Generating wallpapers from vector graphics"
date: "2021-06-07"
---

SVG files are really just XML. That means you can do a lot with them programmatically. I recently rearranged a script I wrote to generate simple two tone wallpapers from SVG files and some of you might get some use out of it.

The wallpaper I've been using for ages was generated with an older version of this script.

![Wallpaper](dandelions_monocon.png)

That's what output generally looks like. The resolution and foreground and background colour variables in the script should be changed as desired.

```
#!/bin/sh

res="1920x1080"
bg="#000000"
fg="#ffffff"

usage() {
    echo "Usage: gen_paper in.svg out.png"
    exit
}

test -n "$1" || usage
test -n "$2" || usage

cp "$1" "$1.use"
sed -i -r "s/(fill|stroke|stop-color)(=|:)(\"?)#([0-9a-fA-F]{6}|[0-9a-fA-F]{3})(\"?)/\1\2\3$fg\5/g" "$1.use"
convert -size "$res" canvas:"$bg" -gravity center -background none \( "$1.use" -resize x400 \) -composite "$2"
rm "$1.use"
```

That's the whole thing. There's not much to it. We create a temporary copy of the SVG to work on, change the colors in it, and use imagemagick to create a composite image with it and a generated background.

The replacement regex is written that way because SVG files can have embedded CSS to set attributes like colour. The colour attributes we're interested in are `fill`, `stroke`, and `stop-color`. These could look like `fill="#000000"` or `fill:#000000` and they could use 3 digit colour codes too. That regex will catch all these cases.

In the `convert` command we set `-gravity center` to make sure the centres of the SVG and background are aligned. If you don't want the SVG in the middle of the wallpaper you can remove this and set it to go somewhere else. It's necessary to set `-background none` or the area the SVG is in will have a different background colour to the one you set in the script.

The SVG can be resized to any size you like. If you set it to be bigger than the output file, imagemagick will make it the size you set it and anything past the edges of the output will be cut off. We use `\( "$1.use" -resize x400 \)` with brackets here to make sure the resize operation is performed on the SVG. If the brackets are omitted the whole image you're generating will be resized. Any valid geometry string can be used for resizing here but if you set both dimensions the SVG will be squashed to fit. The `x400` geometry string I used here will make the SVG 400px high on the output and set the width to whatever is necessary to maintain the original aspect ratio. In a `AxB` geometry string `A` or `B` can be missing and will be automatically set relative to the dimension you did specify to maintain the original aspect ratio.

This script is in my [bin repo](https://gitlab.com/ymber/bin).