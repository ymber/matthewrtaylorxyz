---
title: "Setting up a web server on Debian 10"
date: "2021-05-30"
lastmod: "2022-08-03"
---

At the time of writing, this website runs on a VPS running nginx on Debian 10. Setting up your own web server is remarkably easy these days, especially since Let's Encrypt and certbot were created. I'll demonstrate my own server setup here using this website as an example.

## Pre Setup
Before doing anything on the server itself you should make sure your DNS host records are set up right. For this you'll need the IPv4 and IPv6 addresses of your server. Set up A records to point your domain to the server's IPv4 address and AAAA records for the IPv6 address.

```
A    matthewrtaylor.xyz     144.202.13.218
A    www.matthewrtaylor.xyz 144.202.13.218
AAAA matthewrtaylor.xyz     2001:19f0:5:36e5:5400:3ff:fe59:5091
AAAA www.matthewrtaylor.xyz 2001:19f0:5:36e5:5400:3ff:fe59:5091
```

The records for this site look like that. I also have a pair for `*.matthewrtaylor.xyz` which allows subdomains to work properly. You'll want that too if you plan to have any subdomains.

## Server Setup
If you're connecting to your server over SSH you'll probably want to use keys instead of password authentication. With some VPS providers you can set up SSH keys when your server instance is created, but the general method to set up your keys on the server is to use `ssh-copy-id`.

```
ssh-copy-id -i ~/.ssh/keys/matthewrtaylorxyz root@matthewrtaylor.xyz
```

With key authentication set up there is no need to keep other authentication methods enabled. In `/etc/ssh/sshd_config` on the server you may set `UsePAM` and `PasswordAuthentication` to `no` so the SSH server will only accept keys for authentication. Reload the SSH server with `systemctl reload sshd` to make the changes take effect.

With that done we can set up the website proper. Something to note if you're using a Vultr VPS is that the Debian 10 images they use have been updated with a new default firewall config. By default they now block all inbound traffic except on port 22 (SSH). With any VPS provider you might have unexpected firewall defaults and checking the firewall config on your server should be near the top of your troubleshooting list if you've got connection problems.

```
ufw allow 80
ufw allow 443
```

Here we allow inbound and outbound connections on ports 80 and 443 (HTTP and HTTPS respectively). If you're setting up any other applications on the server you'll need to make sure the relevant ports are open for them. On this server I had to open 25, 587, and 993 for the email server.

Now we can install the web server itself, certbot to set up TLS, and rsync.

```
apt install nginx python-certbot-nginx rsync
```

When you install the debian package for nginx the server will automatically be started. With nginx installed and running, you should see a default nginx web page if you visit your website. One problem I came across was my browser being unable to connect to the site after I reinstalled the server. Purging browser data fixed that for me but your mileage may vary. This is the time to do any network troubleshooting you need to and make sure you can connect to the site normally.

The typical setup for nginx is to store site configs in `/etc/nginx/sites-available` and then symlink them in `/etc/nginx/sites-enabled`. Copy the default config at `/etc/nginx/sites-available/default` and call it something you'll recognize.

```
cp /etc/nginx/sites-available/default /etc/nginx/sites-available/matthewrtaylor
```

The default config we just copied defines a virtual server with a handful of config options. There are a few changes needed in the new config to make it serve your website and not the default nginx page. On the two lines telling it to listen on port 80, the `default_server` string must be removed. This is important and if you leave it there your web server won't behave how you expect when you've got several virtual servers defined. The site root directory should be changed to whatever directory you want to store your website in on the server. Generally this should be a directory in `/var/www`. On the line defining the index files you can remove `index.htm` and `index.nginx-debian.html`. This is just to keep the config clean, you can use whichever index file names you want but `index.html` is a good standard name. The last important change in the virtual server config is on the `server_name` line. In the default config this line is `server_name _;`. The addresses for your website should go here so you have something like `server_name matthewrtaylor.xyz www.matthewrtaylor.xyz;`.

```
server {
    listen 80;
    listen [::]:80;

    root /var/www/matthewrtaylor;

    index index.html;

    server_name matthewrtaylor.xyz www.matthewrtaylor.xyz;

    location / {
        try_files $uri $uri/ =404;
    }
}
```

Your configuration should look something like that.

Now you can create a symlink to the config to enable the site. This will take effect when nginx is reloaded.

```
ln -s /etc/nginx/sites-available/matthewrtaylor /etc/nginx/sites-enabled/matthewrtaylor
systemctl reload nginx
```

The last remaining thing to do is set up TLS on the server. This is very easy now that certbot is around.

```
certbot --nginx
```

The configs we just created for nginx will be used by certbot to detect all the information it needs to generate certificates. It will also modify the nginx configs for you to make sure the website is served over a secure connection. The certbot script will ask you which domains to generate certificates for and whether to redirect HTTP connections to HTTPS. You should generate certificates for all domains and redirect unencrypted connections. A handful of things still need to support HTTP connections but for a new website there is no need to ever serve it over an unecrypted connection.

If you visit your website now you should see a 404 page. In the site config we changed the root directory to your new directory in `/var/www` but we never created that directory or put anything in it. Make that directory now.

```
mkdir /var/www/matthewrtaylor
```

If you create a file in there called `index.html` and write some HTML in it you'll have a real live website.

The reason we installed rsync earlier was to use it for easy file transfer between your machine and the server. If you have a directory like `~/web` on your machine with your website files in it you can easily upload these to the server over a secure SSH connection by using rsync.

```
rsync -r ~/git/web/site/* root@matthewrtaylor.xyz:/var/www/matthewrtaylor
```

And that's everything you need to set up a web server.