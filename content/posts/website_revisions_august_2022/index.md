---
title: "Website migration and email server"
date: "2022-08-03"
---

In the last few months I've wanted to expand this site more but the SSG I wrote last year was never supposed to handle more than a blog. When I wrote SBG I did it specifically to avoid using Hugo but now that I've looked deeper into how Hugo works I realize it's ideal for my use case. I've migrated the existing site to Hugo and future expansions will be much smoother for it. I made a custom Hugo theme in the vein of my old SBG templates and it's on my gitlab at [https://gitlab.com/ymber/hugo_ymber](https://gitlab.com/ymber/hugo_ymber). There's probably more to come on that when I add templates for engineering stuff and design packages.

I've also discontinued the email server I was previously running on this server. The maintenance burden of the server and generally shocking state of email clients' IMAP support were starting to annoy me and the benefits I gained from running the server didn't justify the work. The contact page on this site has been updated with my protonmail and a new GPG key.