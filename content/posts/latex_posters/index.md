---
title: "Making posters with LaTeX"
date: "2022-02-20"
lastmod: "2022-02-20"
---

I've wanted a printed version of Ben Crowder's [Latin declensions chart](https://bencrowder.net/latin-declensions/) for a while. His version is made for US letter paper and I need it for ISO A formats so I looked at the source to make some changes and it turns out rendering it depends on a program that only runs on appleware. I considered making a vector image version so it could be rendered at whatever resolution bigger paper would need but that's a pain to do so I went with LaTeX.

## Poster formatting
It isn't that common to see posters made with LaTeX but it can be done and there are some quite good packages to help with layout. I used `tcolorbox` in poster mode and that served all my needs for this poster. I had to use custom frame drawing code for it as the standard skin couldn't be configured to look like the original declension chart but it turned out that cutting the frame rendering down to a single line was very simple.

```
boxes = {
colframe=white,
colback=white,
coltitle=black,
boxrule=2mm,
skin=freelance,
frame code={\path[draw=lightgrey]([xshift=0.015\textwidth]frame.north west) -- ([xshift=-0.03\textwidth]frame.north east);}
}
```

The example given in [tcolorbox's documentation](https://tools.ietf.org/doc/texlive-doc/latex/tcolorbox/tcolorbox.pdf) gives a good example of how to use `freestyle` for custom frame rendering and everything else about the package is comprehensively documented.

The source for the contents of the poster is pretty cumbersome in places. Defining layout with `posterbox`es and nested tables is pretty clean but the text itself with all its different colouring and font types ends up having far more macros than content. Every word of the examples is defined by something like `\textbf{\textcolor{darkblack}{serv}\textcolor{highlightblue}{ī}}` and there are a lot of those. There's a lot of repetition in there so I might write some macros to shorten it all.

## Results
My version of the poster uses standard fonts in place of the Adobe fonts in the original. Apart from that and some minor formatting differences I've preserved the appearance of the original.

![Latin declension chart](latin_declension_chart.png)

There's a [repository](https://gitlab.com/ymber/latin_declensions_latex) on my Gitlab with all the source files. Ben made some other good Latin grammar charts in a similar vein so I might make some more printable versions of those too.

One of these days when I get the printing site finished I'll put these up on there.

## Rendering and printing at different sizes
To render the poster with different page sizes I've taken the simplest approach and used `pdfpages`. The poster is defined for A4 and for larger sizes the A4 PDF is scaled up to larger pages. The original output has no rasterization so the scaling works fine without any loss of quality.