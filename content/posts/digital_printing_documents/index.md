---
title: "Using LaTeX to produce book printing documents"
date: "2021-06-04"
---

I'm currently using LaTeX to typeset books for printing. The standard `book` document class sets sensible defaults and handles most formatting automatically. Setting up the document for printing has been quite smooth and there were only minor points of difficulty when it came to going from PDF to printed book. I'll document here the sticking points I hit with LaTeX and book design conventions.

## Page size
It isn't immediately obvious how to properly set the page size of the output PDF when you're using `pdflatex`. If you specify a page size option for your document class like `\documentclass[a5paper]{book}` you'll find that when the PDF is generated the page size is still the default `usletter`. I've ended up using the `geometry` package to set the page size. It has some pre defined sizes (`a4paper`, `executivepaper` etc) or you can directly specify width and height like `\usepackage[paperwidth=7in, paperheight=10in]{geometry}`.

My page designs have no need for a bleed area but the printing company I'm using automatically adds a 0.125" margin anyway. Some print shops will print documents exactly as provided and the page size will need to be increased if a bleed area is required. The conventional size is 0.125" so a book with a final page size of 6"x9" would need 6.25"x9.25" pages for printing.

## Margin sizes
I ran into some confusion about the proper size of inner and outer margins in a book. Per the [LaTeX class documentation](https://mirror.ox.ac.uk/sites/ctan.org/macros/latex/base/classes.pdf) the outer margin of a page is supposed to be wider than the inner margin and this is the default for the `book` class with `twoside`.

> If @twoside is true (which is always the case for book) we make the inner margin smaller than the outer one.

This can be complicated by binding offsets where extra space is added to the inner margin for a thicker book where more of the inner margin will effectively be lost in the binding.

## Page parity
There is a convention in book design that the left hand page should always be an even numbered page. The `book` class has `openright` by default to enforce this. The defaults here will ensure that page numbers are on the outer side of the page where they should be and the vertical margins are on the proper side if they're different widths.

## Setting up documents for different formats
I've separated the main content of the book into its own file and used `\input` to pull it into different files for different printing or PDF formats. This means that the content doesn't have to be duplicated in files that only differ by a few lines each for formatting.

```
\documentclass[12pt]{book}
\usepackage[paperwidth=7in, paperheight=10in]{geometry}

\setcounter{secnumdepth}{-1}

\begin{document}
\title{The Meditations of the Emperor Marcus Aurelius Antoninus}
\author{Marcus Aurelius Antoninus
        \and George W. Chrystal}
\date{1902}

\frontmatter
\maketitle

\mainmatter
\input{content.tex}

\end{document}
```

That is the entire content of `book_7x10.tex`. To generate a PDF more suitable to be an ebook it only takes a few changes to the preamble. I removed the `geometry` package and specified the `oneside` and `openany` options for the document class. The defaults of `twoside` and `openright` are not appropriate for a digital document because there is no concept of recto and verso as there is in a printed book. Blank pages to keep page parity right are unnecessary, and the margin sizes and location of page numbers and such should be the same on each page.