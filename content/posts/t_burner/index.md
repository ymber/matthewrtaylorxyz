---
title: "Building a naturally aspirated propane forge burner"
date: "2022-08-29"
---

I've spent the last 4 weeks working on a propane burner for a forge. I didn't want it to be dependent on electricity so blown designs were out. Of all the naturally aspirated designs available the T burner is in theory one of the simplest and it can operate at much lower gas pressures than other NA burners which puts a big dent in fuel consumption.

Frosty's [original T burner designs](https://www.iforgeiron.com/topic/43976-t-burner-illustrated-directions/) were drawn up around American plumbing parts and adapting the design for British components took several weeks of trying parts, finding it's not feasible to fit them together, then waiting another week for new parts to arrive. The build itself is simple, but the experimentation to get a working design together took some doing.

![Finished burner](finished_burner.jpg)

After a while looking at the design document and bore diameter reference tables for British Standard Pipe fittings I realized that the important dimensions for BSP fittings are almost the same as for American NPT fittings. The mixing tube diameter to air intake area ratios were satisfied by parts with the same nominal sizes in either system. For the main body of the burner I could just stick to the parts list as given. The problems started with the gas assembly. In Frosty's design you use a contact tip from a MIG welder as a gas jet nozzle and this is threaded into the inside of a tapped double ended union threaded into the top of the T.

![Mangled 1/8" x 1/4" BSPP union](mangled_union.jpg)

Fitting a MIG tip to a double ended union took some working out. The original design calls for a 1/4"x28 threaded MIG tip to fit in a tapped 1/8" end of a 1/8" NPT x 1/4" flare reducing union. That was where the similarity between British and American parts ended. I could only get MIG tips with an M6 or M8 thread. M6 was too small for the bore diameter of a 1/8" BSP fitting and tapping the same fitting for M8x1.25 cut right through the walls. With perfect alignment it might be possible to tap it without going through the walls but the wall thickness you'd be left with would be really sketchy for a high pressure gas fitting. A much better solution presented itself in the form of a random air line union another guy in the shop found on the bench. It turned out to be a 1/4" BSPP x 1/4" BSPT cone seat union. Being a high pressure pneumatic fitting it had thick walls, which happened to give it a good bore diameter to tap for M8 without having to drill it.

![Gas assemble](gas_assembly.jpg)

From that point the rest of the design for the gas assembly is fairly obvious. Connect the other end of the coned pneumatic adaptor to a hose tail via a double female socket and fit a hose clamp. There's a minor graveyard of parts on the bench now that are mangled or turned out to not be needed but the gas assembly was really simple in the end.

![Burner in operation](forge_running.jpg)

T burners generally need tuning to get them to burn efficiently. How they behave depends on the forge and conditions they're operating in. This one worked remarkably well off the bat. Just by tightening them well, all of the connections between parts are gas tight up to at least 35 PSI and probably higher. Given that a 3/4" T burner has an expected operating range of 6-10 PSI that's definitely sufficient. I suspect the use of a tapered thread to connect the gas assembly to the T makes it easier to get a gas seal than it would be with a parallel thread. The MIG tip has a 1mm bore which is ~11% larger than the 0.035" used in the original design. This contributed to some tuning issues I had right at the start where the air-fuel ratio in the mixing tube was too fuel rich but having spent a while with gas leak check spray and a wrench getting the gas assembly seals tight enough the problems went away. I'd plan to use a 0.8mm MIG tip in future builds. The jet gap between the tip of the jet and the start of the mixing tube is possibly also shorter than is ideal but looking at how the forge is running it appears to be a burning well with a fairly neutral air-fuel ratio.

The top of the burner gets hotter than I'd like given that it has a hose clamped directly to it but as long as the forge is running the pressure away from the top keeps it from getting concerningly hot. Once the forge is off though it can get hot enough up there that hose damage could be a concern. For this setup I can just pull the burner out when I turn it off but it's an issue to be rectified in future designs.

I'm finalizing the tuning for this setup with some help from Frosty and when I've got it settled I'll write up a build document. Most of my experience in adapting this design falls into the category of mistakes you don't need to make yourself if someone else already made them and it might save someone some delivery charges and weeks waiting if I've written it down. That will be the first entry on the new engineering  and plans section of this site.