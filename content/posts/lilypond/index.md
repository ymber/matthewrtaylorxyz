---
title: "High quality vector music engraving with Lilypond"
date: "2022-04-21"
---

I've recently found myself needing to produce printed music sheets. There are LaTeX packages that can handle it but none of them are particularly good and they have pretty narrow feature sets. Enter Lilypond. It's conceptually similar to LaTeX in that it is a system for compiling source files to documents and the workflow feels comfortably familiar if you're used to using LaTeX or similar. The documentation is vast and covers every feature you could possibly need for just about any composition. Despite that the learning curve is pretty easy.

I originally needed it for piano sheets but it will also do tabulature and other things. This is the entire document for a tab for a 3 bar riff I like to play as an intro to Mad World.

```
\version "2.22.2"

\new TabStaff {
  <e, a, e g b g'> g' fis' fis' e' b cis'\3 a, a\4 cis'\3 b e'
}
```

The 6 notes inside the `<>` describe a chord. The `,` and `'` characters are used here as in the Helmholtz notation to specify which octave a note is in. Lilypond provides `\relative` mode where notes are assumed to have their closest pitch to the preceeding note but I avoid using it. It can make things quicker to write for most melodies but it hurts readability in the document source. The Dutch naming scheme for notes is the default in Lilypond and other options are available but I stick with Dutch. The `fis'` and `cis'` notes in that tab are F# and C# respectively. In Dutch -is and -es are used to indicate sharps and flats.

Because a guitar can produce the same note on different strings at different frets the program will sometimes give you a result that isn't what you wanted and it can be something that's just about impossible to actually play. Where I've got `a\4` in the last bar there I'm specifying that the A note should be on the 4th string. With a standard EADGBE tuning you can produce the same A note on the 7th fret of the 4th string or the 2nd fret of the 3rd string. In this riff it's much easier to play it on the 4th string but Lilypond will produce a tab where you play it on string 2 unless you specify.

![Lilypond tabulature rendering](mad_world_riff.png)

The default is to output as a PDF which is good for printing but you can also have PostScript, PNG, and SVG. The man page gives a lot of information about output options and the main docs have more details. Besides producing sheets you can also generate MIDI files and other things.

The versatility Lilypond offers is enormous and it covers more than what I need it for. It's certainly an improvement on the GUI solutions you can get stuck with.