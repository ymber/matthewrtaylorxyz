---
title: "Wrapping up work on the Meditations"
date: "2021-07-04"
lastmod: "2021-07-05"
---

The second proof copy of my new Meditations edition arrived yesterday. This will be the final one for now. I've taken the size down to 6x9 from 7x10 and it's much better. The microtype adjustments to the text are very subtle but they've made it much easier to get the text justified properly.

![Size comparison](meditations_r2_comparison.jpg)

The print quality is the same as in the previous copy I got and there are no notable differences apart from the new size. I was concerned that the inner margins on the document would be too small in 6x9 but they look just right. I was told that the barcode printed on the back of the final page is necessary for the production line so books are bound into the right covers.

![Inside](meditations_r2_inside.jpg)

I'm planning to do my own custom cover document and see if that makes the cover text blur issue go away but apart from that this is the whole book finished. Next up is Latin For Beginners by Benjamin L. D'Ooge from 1911. There's more complex typesetting in that one and I don't have a digital text source so it will take longer than the Meditations did.

Now I need to do the work on my website so people can get copies. Lulu has a print API but it looks like all the existing options to use it are horrendous so I'll probably have to write my own backend code to handle orders. That will be its own adventure.