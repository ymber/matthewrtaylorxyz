---
title: "Building a submersible ROV"
date: "2021-06-22"
---

I've spent most of the last two weeks between my team's lab and the hydrodynamics lab working on a submersible remote vehicle. We've had to go from scratch to fully functioning in under two weeks as it took a very long time for the chassis to arrive and there has been a major mechanical overhaul from previous prototypes. We've dealt with a lot of mechanical problems and more in software so testing has been pretty intense.

![ROV](rov_2021_qualifiers.jpg)

I'm responsible for the embedded systems and control setup for this thing and there's a lot going on in there.

## Overview
The whole thing is controlled from a laptop with an Xbox 360 type game controller. This has to have network sharing set up so we can connect it to a Pi over an ethernet cable. Wireless communication isn't an option here as radio waves degrade very quickly in water. The Pi runs a control server which the laptop connects to as well as a pair of Arduino nano 33 BLE boards. The Pi is mostly there to pass data between the surface and the Arduinos.

## Arduinos
The code running on the Arduinos is probably the simplest part of the embedded systems we've got in here. They control 8 thrusters and 1 other motor. Arduinos being what they are, we've had a lot of problems with them from poor support for PWM outputs to thrusters spinning themselves because of bad signal timings. Most of the problems we've had at this level are because the servo control library is defective and mbed OS isn't written properly. We've also had all the usual problems with Arduino tooling being unacceptably flaky. At this point I want to get rid of the Arduinos and control everything through servo driver boards connected directly to the Pi.

## Pi
The Pi runs a Python control server that passes data between the controls on the surface and the Arduinos. It has a basic static IP setup for networking but this might change in the future with changes on the surface laptop as we've had problems with reliability in the current setup. We're communicating with msgpack between the Pi and the Arduinos because driving down packet sizes directly effects how responsive the controls are for the operator. The improvement over the old code that used JSON isn't enormous but it's enough to matter. The Pi supports Auto-MDIX so we haven't had to deal with different cables here. I'd like to stick with boards that support Auto-MDIX.

## Surface
The control laptop on the surface is running Arch Linux with `xpad` for the controller. This part of it worked pretty much out of the box. Setting up network sharing was the difficult part. I tried a lot of things to get it connected to the Pi with the `iwd` setup I'm used to using, but in the end I had to revert back to `NetworkManager` and its network sharing feature to get a working connection to the Pi. I intend to get network sharing working with `iwd` but for the forseeable future we'll be sticking with `NetworkManager`.

We have plans to go heavily into autonomous operation in the future and this is where all the code for CV neural networks will end up. Frontend code is extremely thin at this point but for the current feature set of the ROV that's fine. We can control the Pi over SSH and most of the surface software can run fine headless.

## Design Changes
I have plans for major changes to the embedded systems next year. Beyond getting rid of the Arduinos, there will be much more need for frontend work as the autonomous control and camera systems are developed. I'm working on making the python code easier to deploy and for next year I want it to have proper test suites. The mechanical engineers are also looking at replacing large amounts of the 6025 aluminium we currently use with acetal or polycarbonate and the thruster configuration is going to change.