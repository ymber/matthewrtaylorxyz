---
title: "Index"
---

# Projects

## Books and classics materials
I'm republishing classical texts that have become hard to come by. Many of them never had digital printing documents and haven't been in print since everything was on copper plates. I'm also making quality printing documents for different materials I found useful learning Latin.

## Aevum Press
I'm building a website to make my publishing project widely accessible. It's snowballed into a custom ecommerce system that handles automating print orders as well as orders from the customer side and everything it needs for the frontend.

## Engineering
As well as the things I'm building for the forge and the hot metalwork tools I'm making, I've got plans for custom machine tools. This site will have a library of design packages for builds I've done.

## Misc
You can find my software on my Gitlab. I maintain a few useful things. I also have a Github account but I only use it for working on public projects hosted there. Some of my packages are in the AUR.